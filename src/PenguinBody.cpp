/*
 * PenguinBody.cpp
 *
 *  Created on: May 10, 2018
 *      Author: TMK
 */

#include <StageState.h>
#include "PenguinBody.h"
#include "Component.h"
#include "Sprite.h"
#include "PenguinCannon.h"
#include "Game.h"
#include "InputManager.h"
#include "Collider.h"
#include "Bullet.h"
#include "Camera.h"
#include "Sound.h"

extern Camera camera;

PenguinBody* PenguinBody::Player = nullptr;

const float PENGUIN_MAX_SPEED = 300.f;
const float PENGUIN_ACELERATION_BY_FRAME = 10.f;
const float PENGUIN_ANGULAR_ACELERATION_BY_FRAME = M_PI/16;

PenguinBody::PenguinBody(GameObject& associated):
		Component(associated),
		speed(Vec2(0,0)),
		linearSpeed(0),
		angle(0),
		hp(10){
	this->associated.AddComponent(new Sprite(this->associated, "assets/img/penguin.png"));

	this->associated.AddComponent(new Collider(this->associated));

	this->Player = this;
}

PenguinBody::~PenguinBody(){
	this->Player = nullptr;
}

void PenguinBody::Start(){
	std::weak_ptr<GameObject> penguinCannonGO = Game::GetInstance().GetCurrentState().AddObject(new GameObject());
	std::weak_ptr<GameObject> penguinBodyAuxPtr = Game::GetInstance().GetCurrentState().GetObjectPtr(&this->associated);
	penguinCannonGO.lock()->AddComponent(new PenguinCannon(*penguinCannonGO.lock(), penguinBodyAuxPtr));
	this->pcannon = penguinCannonGO;

}


void PenguinBody::Update(float dt){

	InputManager& inputManager = InputManager::GetInstance();

	if(inputManager.IsKeyDown(SDLK_w)){
		if(this->speed.Mag() < (PENGUIN_MAX_SPEED - PENGUIN_ACELERATION_BY_FRAME)){
			this->speed.x += PENGUIN_ACELERATION_BY_FRAME * cos(angle);
			this->speed.y += PENGUIN_ACELERATION_BY_FRAME * sin(angle);
		}else{
			if(std::abs(speed.x) > std::abs(speed.y)){
				if(this->speed.x*cos(angle) < 0){
					this->speed.x += PENGUIN_ACELERATION_BY_FRAME * cos(angle);
					this->speed.y += PENGUIN_ACELERATION_BY_FRAME * sin(angle);
				}
			}else if(std::abs(speed.y) > std::abs(speed.x)){
				if(this->speed.y*sin(angle) < 0){
					this->speed.x += PENGUIN_ACELERATION_BY_FRAME * cos(angle);
					this->speed.y += PENGUIN_ACELERATION_BY_FRAME * sin(angle);
				}
			}else{
				if(this->speed.x*cos(angle)*this->speed.y*sin(angle) < 0){
					this->speed.x += PENGUIN_ACELERATION_BY_FRAME * cos(angle);
					this->speed.y += PENGUIN_ACELERATION_BY_FRAME * sin(angle);
				}
			}
		}
	}
	if(inputManager.IsKeyDown(SDLK_s)){
		if(this->speed.Mag() < (PENGUIN_MAX_SPEED - PENGUIN_ACELERATION_BY_FRAME)){
			this->speed.x -= PENGUIN_ACELERATION_BY_FRAME * cos(angle);
			this->speed.y -= PENGUIN_ACELERATION_BY_FRAME * sin(angle);
		}else{
			if(std::abs(speed.x) > std::abs(speed.y)){
				if(this->speed.x*cos(angle) > 0){
					this->speed.x -= PENGUIN_ACELERATION_BY_FRAME * cos(angle);
					this->speed.y -= PENGUIN_ACELERATION_BY_FRAME * sin(angle);
				}
			}else if(std::abs(speed.y) > std::abs(speed.x)){
				if(this->speed.y*sin(angle) > 0){
					this->speed.x -= PENGUIN_ACELERATION_BY_FRAME * cos(angle);
					this->speed.y -= PENGUIN_ACELERATION_BY_FRAME * sin(angle);
				}
			}else{
				if(this->speed.x*cos(angle)*this->speed.y*sin(angle) > 0){
					this->speed.x -= PENGUIN_ACELERATION_BY_FRAME * cos(angle);
					this->speed.y -= PENGUIN_ACELERATION_BY_FRAME * sin(angle);
				}
			}
		}
	}
	if(inputManager.IsKeyDown(SDLK_a)){
		this->angle -= PENGUIN_ANGULAR_ACELERATION_BY_FRAME;
		this->associated.angleDeg = this->angle*180/M_PI;
	}
	if(inputManager.IsKeyDown(SDLK_d)){

		this->angle += PENGUIN_ANGULAR_ACELERATION_BY_FRAME;
		this->associated.angleDeg = this->angle*180/M_PI;
	}

	this->associated.box.x += (this->speed.x/0.33)*dt;
	this->associated.box.y += (this->speed.y/0.33)*dt;

	if(this->associated.box.x < 0){
		this->associated.box.x = 0;
		this->speed.x = 0;
	}else if(this->associated.box.x > (1408 - this->associated.box.w)){
		this->associated.box.x = (1408 - this->associated.box.w);
		this->speed.x = 0;
	}

	if(this->associated.box.y < 0){
		this->associated.box.y = 0;
		this->speed.y = 0;
	}else if(this->associated.box.y > (1280 - this->associated.box.h)){
		this->associated.box.y = (1280 - this->associated.box.h);
		this->speed.y = 0;
	}

}

void PenguinBody::Render(){

}

bool PenguinBody::Is(std::string type){
	return (type=="PenguinBody") ? true : false;
}

void PenguinBody::NotifyCollision(GameObject& other){

	if(other.GetComponent("Bullet")){
		Bullet* auxBullet = static_cast<Bullet*>(other.GetComponent("Bullet"));

		if(auxBullet->targetsPlayer){
			this->hp -= auxBullet->GetDamage();

			if(this->hp <= 0){
				std::weak_ptr<GameObject> explosionGO = Game::GetInstance().GetCurrentState().AddObject(new GameObject());
				explosionGO.lock()->AddComponent(new Sprite(*explosionGO.lock(), "assets/img/penguindeath.png", 5, 0.3, 1.5));
				explosionGO.lock()->AddComponent(new Sound(*explosionGO.lock(), "assets/audio/boom.wav"));

				(explosionGO.lock())->box.x = this->associated.box.RectCenter().x - (explosionGO.lock())->box.w/2;
				(explosionGO.lock())->box.y = this->associated.box.RectCenter().y - (explosionGO.lock())->box.h/2;
				static_cast<Sound*>(explosionGO.lock()->GetComponent("Sound"))->Play(1);

				camera.Unfollow();
				this->pcannon.lock()->RequestDelete();
				this->associated.RequestDelete();

				Game::GetInstance().gameData.endGameState = true;
				Game::GetInstance().gameData.playerVictory = false;
			}

		}
	}

	if(other.GetComponent("Alien")){

		this->hp--;
		if(this->hp <= 0){
			std::weak_ptr<GameObject> explosionGO = Game::GetInstance().GetCurrentState().AddObject(new GameObject());
			explosionGO.lock()->AddComponent(new Sprite(*explosionGO.lock(), "assets/img/penguindeath.png", 5, 0.3, 1.5));
			explosionGO.lock()->AddComponent(new Sound(*explosionGO.lock(), "assets/audio/boom.wav"));

			(explosionGO.lock())->box.x = this->associated.box.RectCenter().x - (explosionGO.lock())->box.w/2;
			(explosionGO.lock())->box.y = this->associated.box.RectCenter().y - (explosionGO.lock())->box.h/2;
			static_cast<Sound*>(explosionGO.lock()->GetComponent("Sound"))->Play(1);

			camera.Unfollow();
			this->pcannon.lock()->RequestDelete();
			this->associated.RequestDelete();

			Game::GetInstance().gameData.endGameState = true;
			Game::GetInstance().gameData.playerVictory = false;
		}
	}

	if(other.GetComponent("Minion")){

		this->hp--;
		if(this->hp <= 0){
			std::weak_ptr<GameObject> explosionGO = Game::GetInstance().GetCurrentState().AddObject(new GameObject());
			explosionGO.lock()->AddComponent(new Sprite(*explosionGO.lock(), "assets/img/penguindeath.png", 5, 0.3, 1.5));
			explosionGO.lock()->AddComponent(new Sound(*explosionGO.lock(), "assets/audio/boom.wav"));
			static_cast<Sound*>(explosionGO.lock()->GetComponent("Sound"))->Play(1);

			(explosionGO.lock())->box.x = this->associated.box.RectCenter().x - (explosionGO.lock())->box.w/2;
			(explosionGO.lock())->box.y = this->associated.box.RectCenter().y - (explosionGO.lock())->box.h/2;

			camera.Unfollow();
			this->pcannon.lock()->RequestDelete();
			this->associated.RequestDelete();

			Game::GetInstance().gameData.endGameState = true;
			Game::GetInstance().gameData.playerVictory = false;
		}
	}
}

Vec2 PenguinBody::GetPosition(){
	return this->associated.box.RectCenter();
}
