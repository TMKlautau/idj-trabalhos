/*
 * Sprite.cpp
 *
 *  Created on: Mar 20, 2018
 *
 */

#include "Sprite.h"
#include "Game.h"
#include "Resources.h"
#include "Camera.h"

extern Camera camera;

extern Resources resources;

Sprite::Sprite(GameObject& associated, std::string file, int frameCount, float frameTime, float secondsToSelfDestruct):
		Component(associated),
		scale(Vec2(1,1)),
		frameCount(frameCount),
		currentFrame(0),
		timeElapsed(0),
		frameTime(frameTime),
		selfDestructCount(),
		secondsToSelfDestruct(secondsToSelfDestruct){
	this->texture = nullptr;

	this->Open(file);
}

Sprite::~Sprite(){

}

void Sprite::Open(std::string file){

	if(this->texture != nullptr){
		SDL_DestroyTexture(this->texture);
	}

	this->texture = resources.GetImage(file);

	if(SDL_QueryTexture(this->texture, nullptr, nullptr , &this->width, &this->height) < 0){
		std::cout << "SDL_queryTexture error: " << SDL_GetError() << std::endl;
	}

	this->SetClip(0,0,this->width/this->frameCount,this->height);

	this->associated.box.w = (this->width/this->frameCount);
	this->associated.box.h = (this->height);

}

void Sprite::SetClip(int x, int y, int w, int h){
	this->clipRect.x = x;
	this->clipRect.y = y;
	this->clipRect.w = w;
	this->clipRect.h = h;
}

void Sprite::Render(){

	SDL_Rect auxRect = SDL_Rect{(int)this->associated.box.x - (int)camera.Pos.x, (int)this->associated.box.y - (int)camera.Pos.y, (int)(this->clipRect.w * this->scale.x), (int)(this->clipRect.h * this->scale.y)};

	SDL_RenderCopyEx(Game::GetInstance().GetRenderer(), this->texture, &this->clipRect, &auxRect, this->associated.angleDeg, nullptr, SDL_FLIP_NONE);

}

void Sprite::Render(float x, float y){

	SDL_Rect auxRect = SDL_Rect{(int)x, (int)y, this->clipRect.w, this->clipRect.h};

	SDL_RenderCopy(Game::GetInstance().GetRenderer(), this->texture, &this->clipRect, &auxRect);

}

int Sprite::GetHeight(){
	return this->height*this->scale.y;
}

int Sprite::GetWidth(){
	return (this->width/this->frameCount)*this->scale.x;
}

bool Sprite::IsOpen(){

	return (this->texture != nullptr)? true : false;

}

void Sprite::Update(float dt){

	this->timeElapsed += dt;

	if(this->timeElapsed > this->frameTime){
		this->timeElapsed -= this->frameTime;
		this->currentFrame++;
		if(this->currentFrame >= this->frameCount){
			this->currentFrame = 0;
			this->SetClip(0,0,(this->width/this->frameCount),this->height);
		}else{
			this->SetClip((this->currentFrame)*this->width/this->frameCount,0,(this->width/this->frameCount),this->height);
		}
	}

	if(this->secondsToSelfDestruct > 0){
		selfDestructCount.Update(dt);
		if(this->selfDestructCount.get() >= this->secondsToSelfDestruct){
			this->associated.RequestDelete();
		}
	}
}

bool Sprite::Is(std::string type){
	return (type=="Sprite") ? true : false;
}

void Sprite::SetScale(float scaleX, float scaleY){
	if(scaleX != 0){
		this->scale.x = scaleX;
		this->associated.box.x -= (this->width*scaleX/2)-(this->associated.box.w/2);
		this->associated.box.w = this->width*scaleX;
	}
	if(scaleY != 0){
		this->scale.y = scaleY;
		this->associated.box.y -= (this->height*scaleY/2)-(this->associated.box.h/2);
		this->associated.box.h = this->height*scaleY;
	}
}

Vec2 Sprite::GetScale(){
	return this->scale;
}

void Sprite::SetFrame(int frame){
	this->currentFrame = frame;
	if(this->currentFrame >= this->frameCount){
		this->currentFrame = 0;
		this->SetClip(0,0,this->width/this->frameCount,this->height);
		std::cout << "error, frame setado fora do limite";
	}else{
		this->SetClip(this->currentFrame*this->width/this->frameCount,0,this->width/this->frameCount,this->height);
	}
}

void Sprite::SetFrameCount(int frameCount){
	this->frameCount = frameCount;
	this->currentFrame = 0;
	this->SetClip(0,0,this->width/this->frameCount,this->height);

	this->associated.box.w = (this->width/this->frameCount)*this->scale.x;
}

void Sprite::SetFrameTime(float frameTime){
	this->frameTime = frameTime;
}
