/*
 * GameObject.cpp
 *
 *  Created on: Apr 6, 2018
 *      Author: TitoKlautau
 */

#include "GameObject.h"
#include "Component.h"
#include <algorithm>
#include <vector>
#include <iostream>

GameObject::GameObject():
		started(false),
		angleDeg(0),
		isDead(false)
		{
}

GameObject::~GameObject(){

	std::vector<Component*>::reverse_iterator it;
	for(it = this->components.rbegin(); it != this->components.rend(); it++){
		delete(*it);
	}
	this->components.clear();
}

void GameObject::Update(float dt){

	std::vector<Component*>::iterator it;
	for(it = this->components.begin(); it != this->components.end(); it++){
		(*it)->Update(dt);
	}
}

void GameObject::Render(){

	std::vector<Component*>::iterator it;
	for(it = this->components.begin(); it != this->components.end(); it++){
		(*it)->Render();
	}
}

void GameObject::RequestDelete(){
	this->isDead = true;
}

void GameObject::AddComponent(Component* cpt){
	this->components.push_back(cpt);
	if(this->started){
		this->components.back()->Start();
	}
}

void GameObject::RemoveComponent(Component* cpt){
	std::vector<Component*>::iterator it;
	it = std::find(this->components.begin(), this->components.end(), cpt);
	if ( it != this->components.end() )
	   this->components.erase(it);
}

Component* GameObject::GetComponent(std::string type){
	std::vector<Component*>::iterator it;
	for(it = this->components.begin(); it != this->components.end(); it++){
		if((*it)->Is(type)){
			return*it;
		}
	}
	return nullptr;
}

bool GameObject::IsDead(){
	return this->isDead;
}

void GameObject::Start(){

	this->started = true;
	std::vector<Component*>::iterator it;
	for(it = this->components.begin(); it != this->components.end(); it++){
		(*it)->Start();
	}

}


void GameObject::NotifyCollision(GameObject& other){

	std::vector<Component*>::iterator it;
	for(it = this->components.begin(); it != this->components.end(); it++){
		(*it)->NotifyCollision(other);
	}

}
