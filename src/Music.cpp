/*
 * Music.cpp
 *
 *  Created on: Mar 20, 2018
 *
 */

#include "Music.h"
#include "Resources.h"

extern Resources resources;

Music::Music(){
	this->music = nullptr;
}

Music::Music(std::string file){
	this->music = nullptr;
	this->Open(file);
}

void Music::Open(std::string file){
	this->music = resources.GetMusic(file);
}

void Music::Play(int times){

	if(this->music != nullptr){
		if( Mix_PlayMusic(this->music , times) < 0){
			std::cout << "MixPlayMusic apresentou erro" << std::endl;
		}
	}

}

void Music::Stop(int msToStop){
	Mix_FadeOutMusic(msToStop);
}

bool Music::IsOpen(){
	return (this->music != nullptr)? true : false;
}

