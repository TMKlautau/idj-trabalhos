/*
 * InputManager.cpp
 *
 *  Created on: May 3, 2018
 *      Author: TMK
 */

#include "InputManager.h"

#define INCLUDE_SDL

#include "SDL_Include.h"

#include <iostream>

InputManager& InputManager::GetInstance(){
	static InputManager inputManager;
	return inputManager;
}

InputManager::InputManager():
		quitRequested(false),
		updateCounter(0),
		mouseX(0),
		mouseY(0){
	for(int i = 0; i < 6; i++){
		this->mouseState[i] = false;
		this->mouseUpdate[i] = 0;
	}
}

InputManager::~InputManager(){

}

void InputManager::Update(){

	SDL_Event event;

	this->quitRequested = false;

	this->updateCounter++;

	SDL_GetMouseState(&this->mouseX, &this->mouseY);

	while (SDL_PollEvent(&event)) {

		if(event.type == SDL_QUIT) {
			this->quitRequested = true;
		}

		if(event.type == SDL_MOUSEBUTTONDOWN) {
			this->mouseState[event.button.button] = true;
			this->mouseUpdate[event.button.button] = this->updateCounter;
		}

		if(event.type == SDL_MOUSEBUTTONUP) {
			this->mouseState[event.button.button] = false;
			this->mouseUpdate[event.button.button] = this->updateCounter;
		}

		if( event.type == SDL_KEYDOWN ) {
			if(event.key.repeat != 1){
				this->keyState[event.key.keysym.sym] = true;
				this->keyUpdate[event.key.keysym.sym] = this->updateCounter;
			}
		}

		if( event.type == SDL_KEYUP ) {
			this->keyState[event.key.keysym.sym] = false;
			this->keyUpdate[event.key.keysym.sym] = this->updateCounter;
		}
	}
}

bool InputManager::KeyPress(int key){
	return (this->keyUpdate[key] == this->updateCounter) ? this->keyState[key] : false;
}

bool InputManager::KeyRelease(int key){
	return (this->keyUpdate[key] == this->updateCounter) ? !this->keyState[key] : false;
}

bool InputManager::IsKeyDown(int key){
	return this->keyState[key];
}

bool InputManager::MousePress(int button){
	return (this->mouseUpdate[button] == this->updateCounter) ? this->mouseState[button] : false;
}

bool InputManager::MouseRelease(int button){
	return (this->mouseUpdate[button] == this->updateCounter) ? !this->mouseState[button] : false;
}

bool InputManager::IsMouseDown(int button){
	return this->mouseState[button];
}

int InputManager::GetMouseX(){
	return this->mouseX;
}

int InputManager::GetMouseY(){
	return this->mouseY;
}

bool InputManager::QuitRequested(){
	return this->quitRequested;
}

