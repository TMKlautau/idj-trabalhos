/*
 * Bullet.cpp
 *
 *  Created on: May 9, 2018
 *      Author: TMK
 */


#include "Bullet.h"
#include "Sprite.h"
#include "Collider.h"
#include "PenguinBody.h"
#include "Alien.h"

Bullet::Bullet(GameObject& associated, float angle, float speedIn, int damage, float maxDistance, std::string sprite, int spriteFrameCount, bool targetsPlayer):
		Component(associated),
		targetsPlayer(targetsPlayer),
		speed(Vec2((speedIn * cos(angle)), -1*(speedIn * sin(angle)))),
		distanceLeft(maxDistance),
		damage(damage){

	this->associated.AddComponent(new Sprite(this->associated, sprite, spriteFrameCount, 0.5));

	this->associated.AddComponent(new Collider(this->associated));

	//Sprite* auxSprite = dynamic_cast<Sprite*>(this->associated.GetComponent("Sprite"));
	//auxSprite->SetScale(5,5);

}

void Bullet::Update(float dt){

	this->distanceLeft -= sqrt(pow(this->speed.x * dt, 2.f) + pow(this->speed.y * dt, 2.f));

	if(this->distanceLeft <= 0){

		//arrumar depois para que o projetil somente ande o q falta

		this->associated.box.x += this->speed.x * dt;

		this->associated.box.y += this->speed.y * dt;

		this->associated.RequestDelete();

	}else{
		this->associated.box.x += this->speed.x * dt;

		this->associated.box.y += this->speed.y * dt;
	}

}

void Bullet::Render(){

}

bool Bullet::Is(std::string type){
	return (type=="Bullet") ? true : false;
}

int Bullet::GetDamage(){
	return this->damage;
}

void Bullet::NotifyCollision(GameObject& other){

	if(other.GetComponent("PenguinBody")){
		if(this->targetsPlayer){
			this->associated.RequestDelete();
		}
	}

	if(other.GetComponent("Alien")){
		if(!this->targetsPlayer){
			this->associated.RequestDelete();
		}
	}
}

