/*
 * Component.cpp
 *
 *  Created on: Apr 6, 2018
 *      Author: TitoKlautau
 */

#include"Component.h"
#include <iostream>

Component::Component(GameObject& associated): associated(associated){

}

Component::~Component(){

}

void Component::Start(){
}

void Component::NotifyCollision(GameObject& other){

}
