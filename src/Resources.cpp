/*
 * Resources.cpp
 *
 *  Created on: Apr 13, 2018
 *      Author: TitoKlautau
 */

#include "Resources.h"
#include "Game.h"

SDL_Texture* Resources::GetImage(std::string file){
	auto search = this->imageTable.find(file);
	if(search != this->imageTable.end()){
		return search->second;
	}else{
		SDL_Texture* texture;
		if((texture = IMG_LoadTexture(Game::GetInstance().GetRenderer(), file.c_str())) == nullptr){
			std::cout << "IMG_LoadTexture error: " << SDL_GetError() << std::endl;
			return nullptr;
		}else{
			this->imageTable.insert({file, texture});
			return texture;
		}
	}
}

Mix_Music* Resources::GetMusic(std::string file){
	auto search = this->musicTable.find(file);
	if(search != this->musicTable.end()){
		return search->second;
	}else{
		Mix_Music* music;
		if((music = Mix_LoadMUS(file.c_str())) == nullptr){
			std::cout << "loadMus apresentou erro" << std::endl;
			return nullptr;
		}else{
			this->musicTable.insert({file, music});
			return music;
		}
	}
}

Mix_Chunk* Resources::GetSound(std::string file){
	auto search = this->soundTable.find(file);
	if(search != this->soundTable.end()){
		return search->second;
	}else{
		Mix_Chunk* chunk;
		chunk = Mix_LoadWAV(file.c_str());
		if(chunk == nullptr){;
			std::cout << "ocorreu erro no sound open" << std::endl;
			return nullptr;
		}else{
			this->soundTable.insert({file, chunk});
			return chunk;
		}
	}
}

TTF_Font* Resources::GetFont(std::string file, int ftsize){
	auto search = this->fontTable.find(file + std::to_string(ftsize));
	if(search != this->fontTable.end()){
		return search->second;
	}else{
		TTF_Font* font;
		font = TTF_OpenFont(file.c_str(),ftsize);
		if(font == nullptr){;
			std::cout << "ocorreu erro no font open" << std::endl;
			return nullptr;
		}else{
			this->fontTable.insert({file + std::to_string(ftsize), font});
			return font;
		}
	}
}

void Resources::ClearImages(){
	std::unordered_map<std::string, SDL_Texture*>::iterator it = this->imageTable.begin();

	while (it != this->imageTable.end()){
		if(it->second != nullptr){
			SDL_DestroyTexture(it->second);
		}
		it++;
	}

	this->imageTable.clear();
}

void Resources::ClearMusics(){
	std::unordered_map<std::string, Mix_Music*>::iterator it = this->musicTable.begin();

	while (it != this->musicTable.end()){
		if(it->second != nullptr){
			Mix_FreeMusic(it->second);
		}
		it++;
	}
	this->musicTable.clear();
}

void Resources::ClearSounds(){
	std::unordered_map<std::string, Mix_Chunk*>::iterator it = this->soundTable.begin();

	while (it != this->soundTable.end()){
		Mix_FreeChunk(it->second);
		it++;
	}
	this->soundTable.clear();
}

void Resources::ClearFonts(){
	std::unordered_map<std::string, TTF_Font*>::iterator it = this->fontTable.begin();

	while (it != this->fontTable.end()){
		TTF_CloseFont(it->second);
		it++;
	}
	this->fontTable.clear();
}

Resources resources;
