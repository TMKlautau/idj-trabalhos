/*
 * State.cpp
 *
 *  Created on: Mar 20, 2018
 *
 */

#include <StageState.h>
#include <cstdlib>
#include "sprite.h"
#include "Sound.h"
#include "Face.h"
#include "Vec2.hpp"
#include "GameObject.h"
#include "TileSet.h"
#include "TileMap.h"
#include "InputManager.h"
#include "Camera.h"
#include "CameraFollower.h"
#include "Alien.h"
#include <iostream>
#include "PenguinBody.h"
#include "Collider.h"
#include "Game.h"
#include "EndState.h"

extern Camera camera;


const float  PI=3.14159265358979f;

StageState::StageState():
		State(),
		backgroundMusic("assets/audio/stageState.ogg"){

	Game::GetInstance().gameData.endGameState = false;

	this->quitRequested = false;

	this->backgroundMusic.Play();

 	this->objectArray.emplace_back(new GameObject());

	this->objectArray.back()->AddComponent(new Sprite(*this->objectArray.back(), "assets/img/ocean.jpg"));
	this->objectArray.back()->AddComponent(new CameraFollower(*this->objectArray.back()));

	this->objectArray.back()->box.x = 0;
	this->objectArray.back()->box.y = 0;
	this->objectArray.back()->box.h = 800;
	this->objectArray.back()->box.w = 1024;

	this->objectArray.emplace_back(new GameObject());

	TileSet* tileSetAux = new TileSet(64, 64, "assets/img/tileset.png", *this->objectArray.back());

	this->objectArray.back()->box.x = 0;
	this->objectArray.back()->box.y = 0;
	this->objectArray.back()->box.h = 64;
	this->objectArray.back()->box.w = 64;

	this->objectArray.back()->AddComponent(new TileMap(*this->objectArray.back(), "assets/map/tileMap.txt", tileSetAux));


	this->objectArray.emplace_back(new GameObject());

	this->objectArray.back()->box.x = 512 - 146/2;
	this->objectArray.back()->box.y = 300 - 100/2;

	this->objectArray.back()->AddComponent(new Alien(*this->objectArray.back(), 7));

	this->objectArray.emplace_back(new GameObject());

	this->objectArray.back()->box.x = 1412 - 146/2;
	this->objectArray.back()->box.y = 700 - 100/2;

	this->objectArray.back()->AddComponent(new Alien(*this->objectArray.back(), 5, 0.5));


	this->objectArray.emplace_back(new GameObject());

	this->objectArray.back()->box.x = 704;
	this->objectArray.back()->box.y = 640;

	this->objectArray.back()->AddComponent(new PenguinBody(*this->objectArray.back()));

	camera.Follow(this->objectArray.back().get());

}

StageState::~StageState(){
	this->objectArray.clear();

	this->backgroundMusic.Stop();
	camera.Unfollow();
	camera.Pos.x = 0;
	camera.Pos.y = 0;

}

void StageState::LoadAssets(){

}

void StageState::Update(float dt){

	InputManager& inputManager = InputManager::GetInstance();

	if(Game::GetInstance().gameData.endGameState){
		this->popRequested = true;
		Game::GetInstance().Push(new EndState());
	}

	this->quitRequested = inputManager.QuitRequested();

	if(inputManager.KeyPress(SDLK_ESCAPE)){
		this->popRequested = true;
	}

	camera.Update(dt);

	for(unsigned int i = 0; i < this->objectArray.size(); i++){
		this->objectArray[i]->Update(dt);
	}

	for(unsigned int i = 0; i < this->objectArray.size() - 1; i++){
		for(unsigned int f = i+1; f < this->objectArray.size(); f++){

			Collider* auxCollider1 = static_cast<Collider*>(this->objectArray[i]->GetComponent("Collider"));
			Collider* auxCollider2 = static_cast<Collider*>(this->objectArray[f]->GetComponent("Collider"));

			if(auxCollider1 && auxCollider2){
				if(    (auxCollider2->box.Contains(auxCollider1->box.x, auxCollider1->box.y))
					|| (auxCollider2->box.Contains(auxCollider1->box.x + auxCollider1->box.w, auxCollider1->box.y))
					|| (auxCollider2->box.Contains(auxCollider1->box.x, auxCollider1->box.y + auxCollider1->box.h))
					|| (auxCollider2->box.Contains(auxCollider1->box.x + auxCollider1->box.w, auxCollider1->box.y + auxCollider1->box.h))
					){
					this->objectArray[i]->NotifyCollision(*this->objectArray[f]);
					this->objectArray[f]->NotifyCollision(*this->objectArray[i]);
				}
		 	}
		}
	}

	for(unsigned int i = 0; i < this->objectArray.size(); i++){
		if(this->objectArray[i]->IsDead()){
			std::vector<std::shared_ptr<GameObject>>::iterator it = this->objectArray.begin();
			this->objectArray.erase(it + i);
		}
	}


}

void StageState::Render(){
	std::vector<std::shared_ptr<GameObject>>::iterator it;
	for(it = this->objectArray.begin(); it != this->objectArray.end(); it++){
		(*it)->Render();
	}
}

void StageState::Start(){
	this->started = true;
	this->LoadAssets();

	std::vector<std::shared_ptr<GameObject>> auxObjectArray = this->objectArray; //iterator estava invalidando pois o vector est� aumento e se realocando

	std::vector<std::shared_ptr<GameObject>>::iterator it;
	for(it = auxObjectArray.begin(); it != auxObjectArray.end(); it++){
		(*it)->Start();
	}
}

void StageState::Pause(){

	this->backgroundMusic.Stop();
	camera.Unfollow();
	camera.Pos.x = 0;
	camera.Pos.y = 0;

}

void StageState::Resume(){

}

