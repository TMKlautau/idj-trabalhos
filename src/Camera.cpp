/*
 * Camera.cpp
 *
 *  Created on: May 4, 2018
 *      Author: TMK
 */


#include "Camera.h"
#include "InputManager.h"
#include <iostream>

void Camera::Follow(GameObject* newFocus){
	this->focus = newFocus;
}

void Camera::Unfollow(){
	this->focus = nullptr;
}

void Camera::Update(float dt){
	this->Speed.x = 300;
	this->Speed.y = 300;
	if(focus != nullptr){
		Vec2 aux = this->focus->box.RectCenter();
		this->Pos.x =  aux.x - 1024/2;
		this->Pos.y =  aux.y - 600/2;
	}else{
		InputManager& inputManager = InputManager::GetInstance();
		if(inputManager.IsKeyDown(SDLK_LEFT)){
			this->Pos.x -= dt*this->Speed.x;
		}
		if(inputManager.IsKeyDown(SDLK_RIGHT)){
			this->Pos.x += dt*this->Speed.x;
		}
		if(inputManager.IsKeyDown(SDLK_UP)){
			this->Pos.y -= dt*this->Speed.y;
		}
		if(inputManager.IsKeyDown(SDLK_DOWN)){
			this->Pos.y += dt*this->Speed.y;
		}
	}
}

Camera camera;
