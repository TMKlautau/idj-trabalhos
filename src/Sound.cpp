/*
 * Sound.cpp
 *
 *  Created on: Apr 6, 2018
 *      Author: TitoKlautau
 */



#include "Sound.h"
#include <SDL2/SDL.h>
#include <iostream>
#include "Resources.h"

extern Resources resources;

Sound::Sound(GameObject& associated): Component(associated), chunk(nullptr), channel(-2){

}

Sound::Sound(GameObject& associated, std::string file):Component(associated), chunk(nullptr), channel(-2){
	this->Open(file);
}

Sound::~Sound(){
	if(this->chunk != nullptr){
		Mix_HaltChannel(this->channel);
	}
}

void Sound::Play(int times){
	if((this->channel = Mix_PlayChannel(-1,this->chunk,times)) == -1){;
		std::cout << "erro no sound play  " << Mix_GetError() << std::endl;
	}
	SDL_Delay(300);
}

void Sound::Stop(){
	if(this->chunk != nullptr){
		Mix_HaltChannel(this->channel);
	}
}

void Sound::Open(std::string file){

	this->chunk = resources.GetSound(file);
}

bool Sound::Is(std::string type){
	return (type=="Sound") ? true : false;
}

void Sound::Update(float dt){

}

void Sound::Render(){

}
