/*
 * Text.cpp
 *
 *  Created on: May 15, 2018
 *      Author: TMK
 */

#include "Text.h"
#include "Game.h"
#include "Camera.h"
#include "Resources.h"

extern Camera camera;
extern Resources resources;

Text::Text(GameObject& associated, std::string fontFile, int fontSize, TextStyle style, std::string text, SDL_Color color):
			Component(associated),
			texture(nullptr),
			text(text),
			style(style),
			fontFile(fontFile),
			fontSize(fontSize),
			color(color){

	this->font = resources.GetFont(fontFile, fontSize);

	this->RemakeTexture();

}

Text::~Text(){
	if(this->texture){
		SDL_DestroyTexture(this->texture);
	}
}

void Text::Update(float dt){

}

void Text::Render(){

	SDL_Rect clipRect = SDL_Rect{0,0,(int)this->associated.box.w, (int)this->associated.box.h};

	SDL_Rect auxRect = SDL_Rect{(int)this->associated.box.x - (int)camera.Pos.x, (int)this->associated.box.y - (int)camera.Pos.y, (int)clipRect.w, (int)clipRect.h};

	SDL_RenderCopyEx(Game::GetInstance().GetRenderer(), this->texture, &clipRect, &auxRect, this->associated.angleDeg, nullptr, SDL_FLIP_NONE);

}

void Text::SetText(std::string text){
	this->text = text;
	this->RemakeTexture();
}

void Text::SetColor(SDL_Color color){
	this->color = color;
	this->RemakeTexture();
}

void Text::SetStyle(TextStyle style){
	this->style = style;
	this->RemakeTexture();
}

void Text::SetFontSize(int fontSize){
	this->fontSize = fontSize;
	this->RemakeTexture();
}

void Text::RemakeTexture(){
	if(this->texture){
		SDL_DestroyTexture(this->texture);
	}

	if((this->font = resources.GetFont(fontFile, fontSize))){
		SDL_Surface* auxSurface = nullptr;
		if(this->style == SOLID){
			auxSurface = TTF_RenderText_Solid(this->font, this->text.c_str(), this->color);
		}else if(this->style == SHADED){
			auxSurface = TTF_RenderText_Shaded(this->font, this->text.c_str(), this->color, SDL_Color{0,0,0,255});
		}else if(this->style == BLENDED){
			auxSurface = TTF_RenderText_Blended(this->font, this->text.c_str(), this->color);
		}else{
			std::cout << "text style inconclusivo" << std::endl;
		}

		if(auxSurface){
			this->texture = SDL_CreateTextureFromSurface(Game::GetInstance().GetRenderer(), auxSurface);
			SDL_FreeSurface(auxSurface);
		}
		int auxW, auxH;
		if(SDL_QueryTexture(this->texture, nullptr, nullptr , &auxW, &auxH) < 0){
			std::cout << "SDL_queryTexture error: " << SDL_GetError() << std::endl;
		}else{
			this->associated.box.h = (float) auxH;
			this->associated.box.w = (float) auxW;
		}
	}
}

bool Text::Is(std::string type){
	return (type=="Text") ? true : false;
}
