/*
 * TileMap.cpp
 *
 *  Created on: Apr 13, 2018
 *      Author: TitoKlautau
 */



#include "TileMap.h"
#include <iostream>
#include <stdio.h>
#include "Camera.h"

extern Camera camera;

TileMap::TileMap(GameObject& associated, std::string file, TileSet* tileSet): // @suppress("Class members should be properly initialized")
	Component(associated),
	tileSet(tileSet)
	{
	this->Load(file);
}

TileMap::~TileMap(){
	delete this->tileSet;
}

void TileMap::Load(std::string file){
	FILE *fp = fopen (file.c_str(), "r");
		if (fp == NULL){
			std::cout << "erro ao abrir arquivo:" <<  file.c_str() << std::endl;
		}
	fscanf (fp, "%d,%d,%d,\n\n", &this->mapWidth, &this->mapHeight, &this->mapDepth);
	tileMatrix.reserve(this->mapWidth*this->mapHeight*this->mapDepth);
	for (int i=0; i<this->mapDepth; i++){
		for (int j=0;j<this->mapHeight;j++){
			fgetc(fp);
			for (int k=0; k<this->mapWidth;k++){
				fscanf (fp, "%d,", &tileMatrix[i*this->mapWidth*this->mapHeight + j*this->mapWidth+k]);
				tileMatrix[i*this->mapWidth*this->mapHeight + j*this->mapWidth+k]--;
				}
			}
		fgetc(fp);
		}
	fclose(fp);
}

void TileMap::SetTileSet(TileSet* tileSet){
	this->tileSet = tileSet;
}

int& TileMap::At(int x, int y, int z){
	return this->tileMatrix[(x + this->mapWidth*y) + z*(this->mapWidth*this->mapHeight)];
}

void TileMap::RenderLayer(int layer, int cameraX, int cameraY){

	for(int i=0; i < this->mapHeight; i++){
		for(int j=0; j < this->mapWidth; j++){
			this->tileSet->RenderTile((unsigned int)this->tileMatrix[(layer*this->mapWidth*this->mapHeight)+(i*this->mapWidth+j)], (float)((j*(this->tileSet->GetTileWidth()))-cameraX), (float)((i*(this->tileSet->GetTileHeight()))-cameraY));
		}
	}

}

void TileMap::Render(){
	for(int i=0; i < this->mapDepth; i++){
		this->RenderLayer(i, camera.Pos.x, camera.Pos.y);
	}
}

int TileMap::GetWidth(){
	return mapWidth;
}

int TileMap::GetHeight(){
	return mapHeight;
}

int TileMap::GetDepth(){
	return mapDepth;
}

bool TileMap::Is(std::string type){
	return type=="TileMap";
}

void TileMap::Update(float dt){
}
