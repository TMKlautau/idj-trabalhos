/*
 * PenguinCannon.cpp
 *
 *  Created on: May 10, 2018
 *      Author: TMK
 */

#include "PenguinCannon.h"
#include "Sprite.h"
#include <cmath>
#include "Camera.h"
#include "InputManager.h"
#include "Game.h"
#include "Bullet.h"
#include "Collider.h"
#include "Timer.h"

extern Camera camera;

const float LENGHT_CANNON = 50.f;

PenguinCannon::PenguinCannon(GameObject& associated, std::weak_ptr<GameObject> penguinBody):
		Component(associated),
		pbody(penguinBody),
		angle(0),
		timer(){
	this->associated.AddComponent(new Sprite(this->associated, "assets/img/cubngun.png"));

	this->associated.AddComponent(new Collider(this->associated));


}


void PenguinCannon::Update(float dt){

	this->timer.Update(dt);

	InputManager& inputManager = InputManager::GetInstance();

	if(this->pbody.lock()){

		this->associated.box.x = (this->pbody.lock()->box.x + this->pbody.lock()->box.w/2) - this->associated.box.w/2;
		this->associated.box.y = (this->pbody.lock()->box.y + this->pbody.lock()->box.h/2) - this->associated.box.h/2;

		this->angle = atan2((inputManager.GetMouseY()  + camera.Pos.y) - this->associated.box.RectCenter().y,(inputManager.GetMouseX() + camera.Pos.x ) - this->associated.box.RectCenter().x);

		this->associated.angleDeg = this->angle *180/M_PI;

		if(inputManager.MousePress(1)){

			if(this->timer.get() > 1){
				this->Shoot();
				this->timer.Restart();
			}
		}

	}else{
		this->associated.RequestDelete();
	}

}

void PenguinCannon::Render(){

}

bool PenguinCannon::Is(std::string type){
	return (type=="PenguinCannon") ? true : false;
}

void PenguinCannon::Shoot(){

	std::weak_ptr<GameObject> bulletGO = Game::GetInstance().GetCurrentState().AddObject(new GameObject());
	(bulletGO.lock())->box.x = this->associated.box.RectCenter().x + LENGHT_CANNON * cos(this->angle);
	(bulletGO.lock())->box.y = this->associated.box.RectCenter().y + LENGHT_CANNON * sin(this->angle);

	(bulletGO.lock())->angleDeg = this->associated.angleDeg;

	(bulletGO.lock())->AddComponent( new Bullet(*bulletGO.lock(), -1* this->angle, 200.f, 1, 1000.f, "assets/img/penguinbullet.png", 4, false));

	(bulletGO.lock())->box.x -= (bulletGO.lock())->box.w/2;
	(bulletGO.lock())->box.y -= (bulletGO.lock())->box.h/2;
}

void PenguinCannon::Start(){

}
