/*
 * EndState.cpp
 *
 *  Created on: May 16, 2018
 *      Author: TMK
 */


#include "EndState.h"
#include "Game.h"
#include "Sprite.h"
#include "InputManager.h"
#include "Text.h"
#include "Music.h"

EndState::EndState():
		textTimer(),
		backgroundMusicPlaying(false){

	this->quitRequested = false;

	if(Game::GetInstance().gameData.playerVictory){
		this->backgroundMusic.Open("assets/audio/endStateWin.ogg");

	 	this->objectArray.emplace_back(new GameObject());

		this->objectArray.back()->AddComponent(new Sprite(*this->objectArray.back(), "assets/img/win.jpg"));

		this->objectArray.back()->box.x = 0;
		this->objectArray.back()->box.y = 0;
		this->objectArray.back()->box.h = 800;
		this->objectArray.back()->box.w = 1024;
	}else{
		this->backgroundMusic.Open("assets/audio/endStateLose.ogg");

	 	this->objectArray.emplace_back(new GameObject());

		this->objectArray.back()->AddComponent(new Sprite(*this->objectArray.back(), "assets/img/lose.jpg"));

		this->objectArray.back()->box.x = 0;
		this->objectArray.back()->box.y = 0;
		this->objectArray.back()->box.h = 800;
		this->objectArray.back()->box.w = 1024;
	}

 	this->objectArray.emplace_back(new GameObject());

	this->objectArray.back()->AddComponent(new Text(*this->objectArray.back(), "assets/font/Call me maybe.ttf", 50, SOLID, "PRESS SPACE FOR NEW GAME OR ESC TO QUIT", SDL_Color{255,165,0,255}));

	this->objectArray.back()->box.x = 75;
	this->objectArray.back()->box.y = 480;
}

EndState::~EndState(){
	this->objectArray.clear();
	this->backgroundMusic.Stop();
}

void EndState::LoadAssets(){

}

void EndState::Update(float dt){

	InputManager& inputManager = InputManager::GetInstance();

	this->textTimer.Update(dt);

	if(!this->backgroundMusicPlaying){
		this->backgroundMusic.Play();
		this->backgroundMusicPlaying = true;
	}

	//o ultimo objeto do array eh sempre o texto

	if(((int)this->textTimer.get())%2 == 0){
		this->objectArray.back()->box.x = 75;
		this->objectArray.back()->box.y = 480;
	}else{
		this->objectArray.back()->box.x = -400;
		this->objectArray.back()->box.y = -480;
	}

	this->quitRequested = inputManager.QuitRequested();
	if(inputManager.KeyPress(SDLK_ESCAPE)){
		this->quitRequested = true;
	}
	if(inputManager.KeyPress(SDLK_SPACE)){
		this->popRequested = true;
	}

}

void EndState::Render(){
	std::vector<std::shared_ptr<GameObject>>::iterator it;
	for(it = this->objectArray.begin(); it != this->objectArray.end(); it++){
		(*it)->Render();
	}
}

void EndState::Start(){
	this->started = true;
	this->LoadAssets();

	std::vector<std::shared_ptr<GameObject>> auxObjectArray = this->objectArray; //iterator estava invalidando pois o vector est� aumento e se realocando

	std::vector<std::shared_ptr<GameObject>>::iterator it;
	for(it = auxObjectArray.begin(); it != auxObjectArray.end(); it++){
		(*it)->Start();
	}
}

void EndState::Pause(){

	this->backgroundMusic.Stop();

}

void EndState::Resume(){

}
