/*
 * Alien.cpp
 *
 *  Created on: May 5, 2018
 *      Author: TMK
 */

#include "Alien.h"
#include "Sprite.h"
#include "InputManager.h"
#include "Camera.h"
#include <cmath>
#include "Game.h"
#include "Minion.h"
#include "GameObject.h"
#include <cstdlib>
#include <ctime>
#include <cmath>
#include <limits>
#include "Collider.h"
#include "Bullet.h"
#include "PenguinBody.h"
#include "Sound.h"

int Alien::alienCount = 0;

Alien::Alien(GameObject& associated, int nMinions, float timeOffset):
		Component(associated),
		hp(10),
		nMinions(nMinions),
		RestTimer(),
		destination(Vec2(0,0)),
		timeOffset(timeOffset){
	this->speed.x = 0;
	this->speed.y = 0;
	this->associated.AddComponent(new Sprite(this->associated, "assets/img/alien.png"));

	this->associated.AddComponent(new Collider(this->associated));

	this->alienCount++;

	this->state = RESTING;
}

void Alien::Start(){

	srand(time(NULL));

	for(int i = 0; i < nMinions; i++){
		std::weak_ptr<GameObject> minionGO = Game::GetInstance().GetCurrentState().AddObject(new GameObject());
		minionGO.lock()->AddComponent(new Minion(*minionGO.lock(), this->associated, (float) i*(2*M_PI/nMinions)));
		minionGO.lock()->angleDeg = 360 - (i*(2*M_PI/nMinions)*180/M_PI);
		Sprite* auxSprite = dynamic_cast<Sprite*>(minionGO.lock().get()->GetComponent("Sprite"));
		int auxRand = (rand()%5);
		auxSprite->SetScale(1+(0.1*auxRand),1+(0.1*auxRand));
		this->minionArray.push_back(minionGO);
	}
}

Alien::~Alien(){
	this->minionArray.clear();
	this->alienCount--;
	if(this->alienCount < 1){
		Game::GetInstance().gameData.endGameState = true;
		Game::GetInstance().gameData.playerVictory = true;
	}
}

void Alien::Update(float dt){

	InputManager& inputManager = InputManager::GetInstance();

	if(inputManager.KeyPress(SDLK_h)){
		this->hp -= 1;
		if(this->hp <= 0){
			std::vector<std::weak_ptr<GameObject>>::iterator it;
			for(it = this->minionArray.begin(); it != this->minionArray.end(); it++){
				std::weak_ptr<GameObject> explosionGO = Game::GetInstance().GetCurrentState().AddObject(new GameObject());
				explosionGO.lock()->AddComponent(new Sprite(*explosionGO.lock(), "assets/img/miniondeath.png", 4, 0.3, 1.2));
				(explosionGO.lock())->box.x = (*it).lock()->box.RectCenter().x - (explosionGO.lock())->box.w/2;
				(explosionGO.lock())->box.y = (*it).lock()->box.RectCenter().y - (explosionGO.lock())->box.h/2;
				(*it).lock()->RequestDelete();
			}
			std::weak_ptr<GameObject> explosionGO = Game::GetInstance().GetCurrentState().AddObject(new GameObject());
			explosionGO.lock()->AddComponent(new Sprite(*explosionGO.lock(), "assets/img/aliendeath.png", 4, 0.3, 1.2));
			explosionGO.lock()->AddComponent(new Sound(*explosionGO.lock(), "assets/audio/boom.wav"));
			static_cast<Sound*>(explosionGO.lock()->GetComponent("Sound"))->Play(1);
			(explosionGO.lock())->box.x = this->associated.box.RectCenter().x - (explosionGO.lock())->box.w/2;
			(explosionGO.lock())->box.y = this->associated.box.RectCenter().y - (explosionGO.lock())->box.h/2;
			this->associated.RequestDelete();

		}
	}

	this->associated.angleDeg += 5;

	if(PenguinBody::Player){

		if(this->state == RESTING){
			if(this->RestTimer.get() < 2+this->timeOffset){
				this->RestTimer.Update(dt);
			}else{
				this->RestTimer.Restart();
				this->destination = Vec2(PenguinBody::Player->GetPosition().x,PenguinBody::Player->GetPosition().y);
				this->state = MOVING;
			}
		}

		if(this->state == MOVING){
			float distX = (this->associated.box.RectCenter().x - this->destination.x);
			float distY = (this->associated.box.RectCenter().y - this->destination.y);
			float auxDistTotal = sqrt(pow(distX, 2.f) + pow(distY, 2.f));
			if(auxDistTotal <= (ALIENMAXSPEED/33)){
				this->associated.box.x = this->destination.x - (this->associated.box.w)/2;
				this->associated.box.y = this->destination.y - (this->associated.box.h)/2;
				this->speed.x = 0;
				this->speed.y = 0;

				this->destination = Vec2(PenguinBody::Player->GetPosition().x,PenguinBody::Player->GetPosition().y);

				float minDist = std::numeric_limits<float>::max();
				std::vector<std::weak_ptr<GameObject>>::iterator indexMinDist = this->minionArray.begin();

				std::vector<std::weak_ptr<GameObject>>::iterator it;
				for(it = this->minionArray.begin(); it != this->minionArray.end(); it++){
					float dist = sqrt(pow((*it).lock()->box.x - this->destination.x, 2) + pow((*it).lock()->box.y - this->destination.y, 2));
					if (minDist > dist){
						minDist = dist;
						indexMinDist = it;
					}
				}
				Minion* auxMinion = dynamic_cast<Minion*>((*indexMinDist).lock().get()->GetComponent("Minion"));
				auxMinion->Shoot(this->destination);

				this->state = RESTING;

			}else if((this->speed.x != 0) || (this->speed.y != 0)){
				this->associated.box.x -= this->speed.x;
				this->associated.box.y -= this->speed.y;
			}	else{
				this->speed.x = distX/(auxDistTotal/(ALIENMAXSPEED/33));
				this->speed.y = distY/(auxDistTotal/(ALIENMAXSPEED/33));
				this->associated.box.x -= this->speed.x;
				this->associated.box.y -= this->speed.y;
			}

		}

	}

	if (hp <= 0){
		this->associated.RequestDelete();
	}
}

void Alien::Render(){

}

bool Alien::Is(std::string type){
	return (type=="Alien") ? true : false;
}


void Alien::NotifyCollision(GameObject& other){

	if(other.GetComponent("Bullet")){
		Bullet* auxBullet = static_cast<Bullet*>(other.GetComponent("Bullet"));

		if(!auxBullet->targetsPlayer){
			this->hp -= auxBullet->GetDamage();
			if(this->hp <= 0){
				std::vector<std::weak_ptr<GameObject>>::iterator it;
				for(it = this->minionArray.begin(); it != this->minionArray.end(); it++){
					std::weak_ptr<GameObject> explosionGO = Game::GetInstance().GetCurrentState().AddObject(new GameObject());
					explosionGO.lock()->AddComponent(new Sprite(*explosionGO.lock(), "assets/img/miniondeath.png", 4, 0.3, 1.2));
					(explosionGO.lock())->box.x = (*it).lock()->box.RectCenter().x - (explosionGO.lock())->box.w/2;
					(explosionGO.lock())->box.y = (*it).lock()->box.RectCenter().y - (explosionGO.lock())->box.h/2;
					(*it).lock()->RequestDelete();
				}
				std::weak_ptr<GameObject> explosionGO = Game::GetInstance().GetCurrentState().AddObject(new GameObject());
				explosionGO.lock()->AddComponent(new Sprite(*explosionGO.lock(), "assets/img/aliendeath.png", 4, 0.3, 1.2));
				explosionGO.lock()->AddComponent(new Sound(*explosionGO.lock(), "assets/audio/boom.wav"));
				static_cast<Sound*>(explosionGO.lock()->GetComponent("Sound"))->Play(1);
				(explosionGO.lock())->box.x = this->associated.box.RectCenter().x - (explosionGO.lock())->box.w/2;
				(explosionGO.lock())->box.y = this->associated.box.RectCenter().y - (explosionGO.lock())->box.h/2;
				this->associated.RequestDelete();
			}
		}
	}

}

