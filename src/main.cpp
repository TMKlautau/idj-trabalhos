/*
 * main.cpp
 *
 *  Created on: Mar 20, 2018
 *
 */

#include "Game.h"
#include "StageState.h"
#include "TitleState.h"

int main(int argc, char* argv[]){

	Game* game = &Game::GetInstance();
	game->Push(new TitleState());
	game->Run();
	return 0;
}

