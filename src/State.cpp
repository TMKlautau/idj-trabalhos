/*
 * State.cpp
 *
 *  Created on: May 15, 2018
 *      Author: TMK
 */

#include "State.h"
#include "iostream"

State::State():
	popRequested(false),
	quitRequested(false),
	started(false){

}

State::~State(){
	this->objectArray.clear();
}

std::weak_ptr<GameObject> State::AddObject(GameObject* go){

	this->objectArray.emplace_back(std::make_shared<GameObject>(*go));

	if(this->started){
		this->objectArray.back()->Start();
	}
	std::weak_ptr<GameObject> aux = this->objectArray.back();
	return aux;
}


std::weak_ptr<GameObject> State::GetObjectPtr(GameObject* go){
	std::vector<std::shared_ptr<GameObject>>::iterator it;
	for(it = this->objectArray.begin(); it != this->objectArray.end(); it++){
		if(it->get() == go){
			std::weak_ptr<GameObject> aux = *it;
			return aux;
		}
	}
	return std::weak_ptr<GameObject>();
}

bool State::PopRequested(){
	return this->popRequested;
}

bool State::QuitRequested(){
	return this->quitRequested;
}

void State::RenderArray(){
	std::vector<std::shared_ptr<GameObject>>::iterator it;
	for(it = this->objectArray.begin(); it != this->objectArray.end(); it++){
		(*it)->Render();
	}
}

void State::StartArray(){
	this->started = true;

	std::vector<std::shared_ptr<GameObject>> auxObjectArray = this->objectArray;
	std::vector<std::shared_ptr<GameObject>>::iterator it;
	for(it = auxObjectArray.begin(); it != auxObjectArray.end(); it++){
		(*it)->Start();
	}
}

void State::UpdateArray(float dt){
	for(unsigned int i = 0; i < this->objectArray.size(); i++){
		this->objectArray[i]->Update(dt);
	}
}
