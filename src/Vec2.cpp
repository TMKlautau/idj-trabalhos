/*
 * Vec2.cpp
 *
 *  Created on: Mar 22, 2018
 *      Author: TMK
 */

#include"Vec2.hpp"

Vec2::Vec2(float x, float y): x(x), y(y){}

Vec2::~Vec2(){

}

void Vec2::Sum(float* Xresult, float* Yresult, float xTerm, float yTerm){
	*Xresult = xTerm + this->x;
	*Yresult = yTerm + this->y;
}

void Vec2::Sub(float* Xresult, float* Yresult, float xTerm, float yTerm){
	*Xresult = xTerm - this->x;
	*Yresult = yTerm - this->y;
}

void Vec2::SProd(float* Xresult, float* Yresult, float scalar){
	*Xresult = scalar * this->x;
	*Yresult = scalar * this->y;
}

float Vec2::Mag(){
	return std::sqrt(((this->x)*(this->x))+((this->y)*(this->y)));
}

void Vec2::Norm(float* Xresult, float* Yresult){
	float aux = this->Mag();
	*Xresult = this->x/aux;
	*Yresult = this->y/aux;
}

float Vec2::Distance(float xTerm, float yTerm){
	return std::sqrt(((this->x-xTerm)*(this->x-xTerm))+((this->y-yTerm)*(this->y-yTerm)));
}

float Vec2::IncRadX(){
	return(atan2(this->y, this->x));
}

float Vec2::IncRadPtV(Vec2 term){

	return(atan2(this->y - term.y,term.x - this->x));
}

void Vec2::RotRad(float* Xresult, float* Yresult, float angle){
	*Xresult = (this->x * cos(angle)) - (this->y * sin(angle));
	*Yresult = (this->y * cos(angle)) - (this->x * sin(angle));
}

Vec2 Vec2::GetRotated(float angle){
	return Vec2((this->x * cos(angle)) - (this->y * sin(angle)),(this->y * cos(angle)) - (this->x * sin(angle)));
}

Vec2 Vec2::operator+(const Vec2 Term){
	return Vec2((Term.x+this->x), (Term.y+this->y));
}
