/*
 * Timer.cpp
 *
 *  Created on: May 11, 2018
 *      Author: TMK
 */



#include "Timer.h"

Timer::Timer():
	time(0.f){

}

void Timer::Update(float dt){
	this->time += dt;
}

void Timer::Restart(){
	this->time = 0.f;
}

float Timer::get(){
	return this->time;
}
