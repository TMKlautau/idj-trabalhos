/*
 * Minion.cpp
 *
 *  Created on: May 9, 2018
 *      Author: TMK
 */

#include "Minion.h"
#include "Sprite.h"
#include "Vec2.hpp"
#include "Bullet.h"
#include "Game.h"
#include <cmath>
#include "Collider.h"

Minion::Minion(GameObject& associated, GameObject& alienCenter, float arcOffsetDeg):
	Component(associated),
	alienCenter(alienCenter),
	arc(arcOffsetDeg){

	this->associated.AddComponent(new Sprite(this->associated, "assets/img/minion.png"));

	this->associated.box.x = this->alienCenter.box.RectCenter().x - this->associated.box.w/2 + (200 * cos(this->arc));

	this->associated.box.y = this->alienCenter.box.RectCenter().y - this->associated.box.h/2 - (200 * sin(this->arc));

	this->associated.AddComponent(new Collider(this->associated));

}

void Minion::Update(float dt){

	this->arc += (2*(M_PI))/(33*10);

	this->associated.angleDeg -= ((2*(M_PI))/(33*10)) * 180/M_PI;

	this->associated.box.x = this->alienCenter.box.RectCenter().x - this->associated.box.w/2 + (200 * cos(this->arc));

	this->associated.box.y = this->alienCenter.box.RectCenter().y - this->associated.box.h/2 - (200 * sin(this->arc));

}

void Minion::Render(){

}

bool Minion::Is(std::string type){
	return (type=="Minion") ? true : false;
}

void Minion::Shoot(Vec2 target){

	std::weak_ptr<GameObject> bulletGO = Game::GetInstance().GetCurrentState().AddObject(new GameObject());
	(bulletGO.lock())->box.x = this->associated.box.RectCenter().x;
	(bulletGO.lock())->box.y = this->associated.box.RectCenter().y;

	(bulletGO.lock())->angleDeg = atan2( bulletGO.lock()->box.y - target.y, bulletGO.lock()->box.x - target.x	)*180/M_PI + 180;
	(bulletGO.lock())->AddComponent( new Bullet(*bulletGO.lock(), this->associated.box.RectCenter().IncRadPtV(target), 200.f, 1, 1000.f, "assets/img/minionbullet2.png", 3, true));

}


void Minion::Start(){

}
