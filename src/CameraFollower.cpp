/*
 * CameraFollower.cpp
 *
 *  Created on: May 4, 2018
 *      Author: TMK
 */


#include "CameraFollower.h"
#include "Camera.h"
#include <iostream>

extern Camera camera;

CameraFollower::CameraFollower(GameObject& associated): Component(associated){}

void CameraFollower::Update(float dt){
	this->associated.box.x = camera.Pos.x;
	this->associated.box.y = camera.Pos.y;
}

void CameraFollower::Render(){

}

bool CameraFollower::Is(std::string type){
	return (type=="CameraFollower") ? true : false;
}
