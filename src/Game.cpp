/*
 * Game.cpp
 *
 *  Created on: Mar 20, 2018
 *
 */

#define INCLUDE_SDL_IMAGE
#define INCLUDE_SDL_MIXER
#include  "Game.h"
#include "Resources.h"
#include "InputManager.h"
#include <SDL2/SDL_ttf.h>

extern Resources resources;

Game* Game::instance = nullptr;

Game::Game(std::string title, int width, int height):
		gameData(),
		frameStart(SDL_GetTicks()),
		dt(0){

	if(instance != nullptr){
		std::cout << "trying to create second game object" << std::endl;
	}else{
		instance = this;
	}

	if( SDL_Init(SDL_INIT_VIDEO|SDL_INIT_AUDIO|SDL_INIT_TIMER) != 0 ){
		std::cout << "SDL_Init error: " << SDL_GetError() << std::endl;
	}

	if( IMG_Init(IMG_INIT_JPG | IMG_INIT_PNG | IMG_INIT_TIF) != (IMG_INIT_JPG | IMG_INIT_PNG | IMG_INIT_TIF)){
		std::cout << "IMG_Init error: " << IMG_GetError() << std::endl; //IMG_GetError is a macro for SDL_GetError
	}

	if( Mix_Init(0) != (0)){
		std::cout << "Mix_Init error: " << Mix_GetError() << std::endl; //Mix_GetError is a macro for SDL_GetError
	}

	if(Mix_OpenAudio(MIX_DEFAULT_FREQUENCY, MIX_DEFAULT_FORMAT, MIX_DEFAULT_CHANNELS, 1024) != 0 ){
		std::cout << "Mix_OpenAudio error: " << Mix_GetError() << std::endl; //Mix_GetError is a macro for SDL_GetError
	}

	Mix_AllocateChannels(32);

	if((this->window = SDL_CreateWindow(title.c_str(), SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, width, height, 0)) == nullptr ){
		std::cout << "SDL_CreateWindow error: " << SDL_GetError()  << std::endl;
	}

	if(this->window == nullptr){
		std::cout << "window nao iniciada corretamente" << std::endl;
	}

	if((this->renderer = SDL_CreateRenderer(this->window, -1, SDL_RENDERER_ACCELERATED)) == nullptr){
		std::cout << "SDL_CreateRenderer error: " << SDL_GetError()  << std::endl;
	}

	if(this->renderer == nullptr){
		std::cout << "renderer nao iniciada corretamente" << std::endl;
	}

	this->storedState = nullptr;

	if(TTF_Init() != 0){
		std::cout << "TTF_Init error: " << TTF_GetError() << std::endl;
	}

	this->gameData.endGameState = false;

}

Game::~Game(){

	resources.ClearImages();

	resources.ClearMusics();

	resources.ClearSounds();

	resources.ClearFonts();

	if(this->storedState){
		delete this->storedState;
	}

	while(!this->stateStack.empty()){
		this->stateStack.pop();
	}

	TTF_Quit();

	SDL_DestroyRenderer(this->renderer);

	SDL_DestroyWindow(this->window);

	Mix_CloseAudio();

	Mix_Quit();

	IMG_Quit();

	SDL_Quit();
}


State& Game::GetCurrentState(){
	return *(this->stateStack.top().get());
}

void Game::Push(State* state){
	this->storedState = state;
}

SDL_Renderer* Game::GetRenderer(){
	return this->renderer;
}

void Game::Run(){

	InputManager& inputManager = InputManager::GetInstance();

	if(this->storedState){
		std::unique_ptr<State> auxPtr(this->storedState);

		this->stateStack.push(std::move(auxPtr));

	 	this->storedState = nullptr;

	 	this->stateStack.top()->Start();
	}

	while(!this->stateStack.empty()){

		while(!this->stateStack.top()->QuitRequested()){

			if(this->stateStack.top()->PopRequested()){
				this->stateStack.pop();
				continue;
			}

			if(this->storedState){

				if(!this->stateStack.empty()){
					this->stateStack.top()->Pause();
				}

				std::unique_ptr<State> auxPtr(this->storedState);

				this->stateStack.push(std::move(auxPtr));

			 	this->storedState = nullptr;

			 	this->stateStack.top()->Start();
			}

			this->CalculateDeltaTime();

			inputManager.Update();

			this->stateStack.top()->Update(this->dt);

			this->stateStack.top()->Render();

			SDL_RenderPresent(this->renderer);
			SDL_Delay(33);
		}
		break;
	}
}

Game& Game::GetInstance(){
	if(instance == nullptr){
		instance = new Game("Tito110142390",1024,600);
	}
	return *instance;
}

void Game::CalculateDeltaTime(){
	int aux = SDL_GetTicks();
	this->dt = aux - this->frameStart;
	this->frameStart = aux;
	this->dt = this->dt/1000;
}

float Game::GetDeltaTime(){
	return this->dt;
}
