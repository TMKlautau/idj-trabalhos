/*
 * Rect.cpp
 *
 *  Created on: Mar 22, 2018
 *      Author: TMK
 */

#include "Rect.h"


Rect::Rect(float x, float y, float w, float h): x(x), y(y), w(w), h(h){}

Rect::~Rect(){

}

void Rect::SumVec2(float* Xresult, float* Yresult, float* Wresult, float* Hresult, float xTerm, float yTerm){
	*Xresult = xTerm+this->x;
	*Yresult = yTerm+this->y;
	*Wresult = this->w;
	*Hresult = this->h;
}

Vec2 Rect::RectCenter(){
	return Vec2(this->x + (this->w/2), this->y + (this->h/2));
}

bool Rect::Contains(float xTerm, float yTerm){
	bool xAxis = (xTerm > this->x) && (xTerm < (this->x+this->w));

	bool yAxis = (yTerm > this->y) && (yTerm < (this->y+this->h));

	return (xAxis && yAxis);
}
