/*
 * Face.cpp
 *
 *  Created on: Apr 6, 2018
 *      Author: TitoKlautau
 */

#include "Face.h"
#include "Sound.h"
#include "Component.h"
#include "InputManager.h"
#include "Camera.h"
#include <iostream>

extern Camera camera;


Face::Face(GameObject& associated): Component(associated), hitpoints(30){}


void Face::Damage(int damage){
	this->hitpoints -= damage;
	if(this->hitpoints <= 0){
		static_cast<Sound*>(this->associated.GetComponent("Sound"))->Play(1);
		this->associated.RequestDelete();
	}
}

void Face::Update(float dt){

	InputManager& inputManager = InputManager::GetInstance();
	if(inputManager.MousePress(1)){
		if(this->associated.box.Contains((inputManager.GetMouseX() + camera.Pos.x), (inputManager.GetMouseY() + camera.Pos.y))){
			this->Damage(std::rand() % 10 + 10);
		}
	}

}

void Face::Render(){

}

bool Face::Is(std::string type){
	return (type=="Face") ? true : false;
}
