/*
 * TileSet.cpp
 *
 *  Created on: Apr 13, 2018
 *      Author: TitoKlautau
 */



#include "TileSet.h"


TileSet::TileSet(int tileWidth, int tileHeight, std::string file, GameObject& GO):
	tileSet(GO, file),
	tileWidth(tileWidth),
	tileHeight(tileHeight)
	{
	if(this->tileSet.IsOpen()){
		this->rows = this->tileSet.GetHeight() / this->tileHeight;
		this->columns = this->tileSet.GetWidth() / this->tileWidth;
	}
}

void TileSet::RenderTile(unsigned index, float x, float y){
	if((index >= 0)&&(index < (unsigned)(this->rows*this->columns))){
		this->tileSet.SetClip(  ((index)%this->columns)*this->tileWidth,
								((index)/this->columns)*this->tileHeight,
								this->tileWidth,
								this->tileHeight);
		this->tileSet.Render(x,y);

	}
}

int TileSet::GetTileWidth(){
	return this->tileWidth;
}

int TileSet::GetTileHeight(){
	return this->tileHeight;
}
