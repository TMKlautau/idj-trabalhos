/*
 * TitleState.cpp
 *
 *  Created on: May 15, 2018
 *      Author: TMK
 */


#include "TitleState.h"
#include "Sprite.h"
#include "InputManager.h"
#include "Game.h"
#include "StageState.h"
#include "Text.h"

TitleState::TitleState():
		textTimer(){
 	this->objectArray.emplace_back(new GameObject());

	this->objectArray.back()->AddComponent(new Sprite(*this->objectArray.back(), "assets/img/title.jpg"));

	this->objectArray.back()->box.x = 0;
	this->objectArray.back()->box.y = 0;

 	this->objectArray.emplace_back(new GameObject());

	this->objectArray.back()->AddComponent(new Text(*this->objectArray.back(), "assets/font/Call me maybe.ttf", 50, SOLID, "PRESS SPACE", SDL_Color{255,165,0,255}));

	this->objectArray.back()->box.x = 400;
	this->objectArray.back()->box.y = 480;
}

TitleState::~TitleState(){

}

void TitleState::LoadAssets(){

}

void TitleState::Update(float dt){
	InputManager& inputManager = InputManager::GetInstance();

	this->textTimer.Update(dt);

	//o ultimo objeto do array eh sempre o texto

	if(((int)this->textTimer.get())%2 == 0){
		this->objectArray.back()->box.x = 400;
		this->objectArray.back()->box.y = 480;
	}else{
		this->objectArray.back()->box.x = -400;
		this->objectArray.back()->box.y = -480;
	}

	this->quitRequested = inputManager.QuitRequested();
	if(inputManager.KeyPress(SDLK_ESCAPE)){
		this->quitRequested = true;
	}
	if(inputManager.KeyPress(SDLK_SPACE)){
		Game::GetInstance().Push(new StageState());
	}
}


void TitleState::Render(){
	std::vector<std::shared_ptr<GameObject>>::iterator it;
	for(it = this->objectArray.begin(); it != this->objectArray.end(); it++){
		(*it)->Render();
	}
}

void TitleState::Start(){
	this->started = true;
	this->LoadAssets();

	std::vector<std::shared_ptr<GameObject>> auxObjectArray = this->objectArray; //iterator estava invalidando pois o vector est� aumento e se realocando

	std::vector<std::shared_ptr<GameObject>>::iterator it;
	for(it = auxObjectArray.begin(); it != auxObjectArray.end(); it++){
		(*it)->Start();
	}
}

void TitleState::Pause(){
}

void TitleState::Resume(){
}
