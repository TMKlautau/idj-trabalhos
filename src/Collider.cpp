/*
 * Collider.cpp
 *
 *  Created on: May 11, 2018
 *      Author: TMK
 */

#include "Collider.h"

Collider::Collider(GameObject& associated, Vec2 scale, Vec2 offset):
	Component(associated),
	scale(scale),
	offset(offset){

}

void Collider::Update(float dt){

	this->box.w = this->associated.box.w * this->scale.x;

	this->box.h = this->associated.box.h * this->scale.y;

	this->box.x = (this->associated.box.x + this->associated.box.w/2) - this->box.w/2;

	this->box.y = (this->associated.box.y + this->associated.box.h/2) - this->box.h/2;

}

void Collider::Render(){

}

bool Collider::Is(std::string type){
	return (type=="Collider") ? true : false;
}

void Collider::SetScale(Vec2 scale){
	this->scale = scale;
}

void Collider::SetOffset(Vec2 offset){
	this->offset = offset;
}

