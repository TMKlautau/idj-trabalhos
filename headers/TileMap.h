/*
 * TileMap.h
 *
 *  Created on: Apr 13, 2018
 *      Author: TitoKlautau
 */

#ifndef SOURCE_HEADERS_TILEMAP_H_
#define SOURCE_HEADERS_TILEMAP_H_

#include "Component.h"

#include "TileSet.h"

class TileMap:public Component{
public:

	TileMap(GameObject& associated, std::string file, TileSet* tileSet);

	~TileMap();

	void Load(std::string file);

	void SetTileSet(TileSet* tileSet);

	int& At(int x, int y, int z = 0);

	void Render();

	void RenderLayer(int layer, int cameraX = 0, int cameraY = 0);

	int GetWidth();

	int GetHeight();

	int GetDepth();

	bool Is(std::string type);

	void Update(float dt);

private:

	std::vector<int> tileMatrix;

	TileSet* tileSet;

	int mapWidth;

	int mapHeight;

	int mapDepth;
};



#endif /* SOURCE_HEADERS_TILEMAP_H_ */
