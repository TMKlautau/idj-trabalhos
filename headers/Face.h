/*
 * Face.h
 *
 *  Created on: Apr 6, 2018
 *      Author: TitoKlautau
 */

#ifndef SOURCE_HEADERS_FACE_H_
#define SOURCE_HEADERS_FACE_H_


#include "GameObject.h"

class Component;

class Face: public Component{
public:
	Face(GameObject& associated);

	void Damage(int damage);

	void Update(float dt);

	void Render();

	bool Is(std::string type);

private:

	int hitpoints;
};



#endif /* SOURCE_HEADERS_FACE_H_ */
