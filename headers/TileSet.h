/*
 * TileSet.h
 *
 *  Created on: Apr 13, 2018
 *      Author: TitoKlautau
 */

#ifndef SOURCE_HEADERS_TILESET_H_
#define SOURCE_HEADERS_TILESET_H_

#include <string>
#include "Sprite.h"
#include "GameObject.h"

class TileSet{
public:
	TileSet(int tileWidth, int tileHeight, std::string file, GameObject& GO);

	void RenderTile(unsigned index, float x, float y);

	int GetTileWidth();

	int GetTileHeight();

private:

	Sprite tileSet;

	int rows;

	int columns;

	int tileWidth;

	int tileHeight;
};



#endif /* SOURCE_HEADERS_TILESET_H_ */
