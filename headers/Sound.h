/*
 * Sound.h
 *
 *  Created on: Apr 6, 2018
 *      Author: TitoKlautau
 */

#ifndef SOURCE_HEADERS_SOUND_H_
#define SOURCE_HEADERS_SOUND_H_

#include "Component.h"
#include "SDL2/SDL_mixer.h"
#include <iostream>

class Sound:public Component{
public:

	Sound(GameObject& associated);
	Sound(GameObject& associated, std::string file);

	~Sound();

	void Play(int times = 0);

	void Stop();

	void Open(std::string file);

	bool IsOpen();

	void Update(float dt);

	void Render();

	bool Is(std::string type);

private:

	Mix_Chunk* chunk;

	int channel;

};



#endif /* SOURCE_HEADERS_SOUND_H_ */
