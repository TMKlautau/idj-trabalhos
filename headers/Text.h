/*
 * Text.h
 *
 *  Created on: May 15, 2018
 *      Author: TMK
 */

#ifndef SOURCE_HEADERS_TEXT_H_
#define SOURCE_HEADERS_TEXT_H_



#define INCLUDE_SDL
#define INCLUDE_SDL_IMAGE
#define INCLUDE_SDL_MIXER
#include "SDL_Include.h"

#include "Component.h"
#include "GameObject.h"
#include <SDL2/SDL_ttf.h>

enum TextStyle {SOLID, SHADED, BLENDED};

class Text: public Component{
public:

	Text(GameObject& associated, std::string fontFile, int fontSize, TextStyle style, std::string text, SDL_Color color);
	~Text();

	void Update(float dt);
	void Render();
	bool Is(std::string type);

	void SetText(std::string text);
	void SetColor(SDL_Color color);
	void SetStyle(TextStyle style);
	void SetFontFile(std::string fontFile);
	void SetFontSize(int fontSize);

private:

	void RemakeTexture();

	TTF_Font* font;
	SDL_Texture* texture;

	std::string text;
	TextStyle style;
	std::string fontFile;
	int fontSize;
	SDL_Color color;

};



#endif /* SOURCE_HEADERS_TEXT_H_ */
