/*
 * Music.h
 *
 *  Created on: Mar 20, 2018
 *
 */

#ifndef SOURCE_HEADERS_MUSIC_H_
#define SOURCE_HEADERS_MUSIC_H_

//#define INCLUDE_SDL
//#define INCLUDE_SDL_MIXER
//#include "SDL_Include.h"

#include <iostream>

#include "SDL2/SDL_mixer.h"

class Music{

public:

	Music();

	Music(std::string file);

	void Play(int times = -1);

	void Stop(int msToStop = 0);

	void Open(std::string file);

	bool IsOpen();

private:

	Mix_Music *music;

};



#endif /* SOURCE_HEADERS_MUSIC_H_ */
