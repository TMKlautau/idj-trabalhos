/*
 * EndState.h
 *
 *  Created on: May 16, 2018
 *      Author: TMK
 */

#ifndef SOURCE_HEADERS_ENDSTATE_H_
#define SOURCE_HEADERS_ENDSTATE_H_

#include "State.h"
#include "Music.h"
#include "Timer.h"

class EndState:public State{
public:
	EndState();
	~EndState();

	void LoadAssets();
	void Update(float dt);
	void Render();

	void Start();
	void Pause();
	void Resume();

private:

	Timer textTimer;

	Music backgroundMusic;

	bool backgroundMusicPlaying;

};



#endif /* SOURCE_HEADERS_ENDSTATE_H_ */
