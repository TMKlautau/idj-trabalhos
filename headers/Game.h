/*
 * Game.h
 *
 *  Created on: Mar 20, 2018
 *
 */

#ifndef SOURCE_HEADERS_GAME_H_
#define SOURCE_HEADERS_GAME_H_

#define INCLUDE_SDL
#include "SDL_Include.h"

#include <iostream>

#include <memory>
#include <State.h>
#include "Resources.h"
#include <stack>
#include <vector>


struct SDL_Window;
struct SDL_Renderer;

class GameData{
public:
	bool endGameState;
	bool playerVictory;
};

class Game{
public:

	Game(std::string title, int width, int height);

	~Game();

	static Game& GetInstance();
	SDL_Renderer* GetRenderer();
	State& GetCurrentState();

	void Push(State* state);

	void Run();

	float GetDeltaTime();

	GameData gameData;

private:

	void CalculateDeltaTime();

	static Game* instance;

	SDL_Window* window;

	SDL_Renderer* renderer;

	State* storedState;

	std::stack< std::unique_ptr<State> > stateStack;

	int frameStart;

	float dt;

};



#endif /* SOURCE_HEADERS_GAME_H_ */
