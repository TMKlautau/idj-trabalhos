/*
 * GameObject.h
 *
 *  Created on: Apr 6, 2018
 *      Author: TitoKlautau
 */

#ifndef SOURCE_HEADERS_GAMEOBJECT_H_
#define SOURCE_HEADERS_GAMEOBJECT_H_

#include <vector>
#include "Rect.h"
#include "Component.h"

class Component;

class GameObject{
public:
	GameObject();
	~GameObject();

	void Update(float dt);

	void Render();

	bool IsDead();

	void RequestDelete();

	void AddComponent(Component* cpt);

	void RemoveComponent(Component* cpt);

	void Start();

	Component* GetComponent(std::string type);

	Rect box;

	bool started;

	double angleDeg;

	void NotifyCollision(GameObject& other);

private:

	std::vector<Component*> components;

	bool isDead;


};



#endif /* SOURCE_HEADERS_GAMEOBJECT_H_ */
