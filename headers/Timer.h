/*
 * Timer.h
 *
 *  Created on: May 11, 2018
 *      Author: TMK
 */

#ifndef SOURCE_HEADERS_TIMER_H_
#define SOURCE_HEADERS_TIMER_H_

class Timer{
public:
	Timer();

	void Update(float dt);

	void Restart();

	float get();

private:

	float time;
};



#endif /* SOURCE_HEADERS_TIMER_H_ */
