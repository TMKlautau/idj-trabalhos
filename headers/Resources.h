/*
 * Resources.h
 *
 *  Created on: Apr 13, 2018
 *      Author: TitoKlautau
 */

#ifndef SOURCE_HEADERS_RESOURCES_H_
#define SOURCE_HEADERS_RESOURCES_H_

#define INCLUDE_SDL
#define INCLUDE_SDL_IMAGE
#define INCLUDE_SDL
#include "SDL_Include.h"
#include "SDL2/SDL_mixer.h"
#include <SDL2/SDL_ttf.h>

#include <unordered_map>
#include <iostream>

class Resources{
public:
	SDL_Texture* GetImage(std::string file);
	void ClearImages();

	Mix_Music* GetMusic(std::string file);
	void ClearMusics();

	Mix_Chunk* GetSound(std::string file);
	void ClearSounds();

	TTF_Font* GetFont(std::string file, int ftsize);
	void ClearFonts();

private:

	std::unordered_map<std::string,SDL_Texture*> imageTable;
	std::unordered_map<std::string,Mix_Music*> musicTable;
	std::unordered_map<std::string,Mix_Chunk*> soundTable;
	std::unordered_map<std::string,TTF_Font*> fontTable;

};



#endif /* SOURCE_HEADERS_RESOURCES_H_ */
