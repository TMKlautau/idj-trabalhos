/*
 * Bullet.h
 *
 *  Created on: May 9, 2018
 *      Author: TMK
 */

#ifndef SOURCE_HEADERS_BULLET_H_
#define SOURCE_HEADERS_BULLET_H_

#include "Component.h"

class Bullet: public Component{
public:

	Bullet(GameObject& associated, float angle, float speedIn, int damage, float maxDistance, std::string sprite, int spriteFrameCount, bool targetsPlayer);

	void Update(float dt);

	void Render();

	bool Is(std::string type);

	int GetDamage();

	void NotifyCollision(GameObject& other);

	bool targetsPlayer;
private:

	Vec2 speed;

	float distanceLeft;

	int damage;

};



#endif /* SOURCE_HEADERS_BULLET_H_ */
