/*
 * Vec2.hpp
 *
 *  Created on: Mar 22, 2018
 *      Author: TMK
 */

#ifndef SOURCE_HEADERS_VEC2_HPP_
#define SOURCE_HEADERS_VEC2_HPP_

#include <cmath>

class Vec2{

public:

	explicit Vec2(float x=0, float y=0);

	~Vec2();

	void Sum(float* Xresult, float* Yresult, float xTerm, float yTerm);

	void Sub(float* Xresult, float* Yresult, float xTerm, float yTerm);

	void SProd(float* Xresult, float* Yresult, float scalar);

	float Mag();

	void Norm(float* Xresult, float* Yresult);

	float Distance(float xTerm, float yTerm);

	float IncRadX();

	float IncRadPtV(Vec2 term);

	float x, y;

	void RotRad(float* Xresult, float* Yresult, float angle);

	Vec2 GetRotated(float angle);

	Vec2 operator+(const Vec2 Term);

};



#endif /* SOURCE_HEADERS_VEC2_HPP_ */
