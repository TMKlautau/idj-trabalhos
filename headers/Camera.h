/*
 * Camera.h
 *
 *  Created on: May 4, 2018
 *      Author: TMK
 */

#ifndef SOURCE_HEADERS_CAMERA_H_
#define SOURCE_HEADERS_CAMERA_H_

#include "GameObject.h"
#include "Vec2.hpp"


class Camera{
public:
	void Follow(GameObject* newFocus);

	void Unfollow();

	void Update(float dt);

	Vec2 Pos;

	Vec2 Speed;

private:

	GameObject* focus;
};



#endif /* SOURCE_HEADERS_CAMERA_H_ */
