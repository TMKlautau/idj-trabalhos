/*
 * Sprite.h
 *
 *  Created on: Mar 20, 2018
 *
 */

#ifndef SOURCE_HEADERS_SPRITE_H_
#define SOURCE_HEADERS_SPRITE_H_

#define INCLUDE_SDL
#define INCLUDE_SDL_IMAGE
#include "SDL_Include.h"

#include <iostream>
#include "component.h"
#include "Timer.h"

class Sprite: public Component{
public:
	Sprite(GameObject& associated, std::string file, int frameCount = 1, float frameTime = 1, float secondsToSelfDestruct = 0);

	~Sprite();

	void Open(std::string file);

	void SetClip(int x, int y, int w, int h);

	void Render();

	void Render(float x, float y);

	int GetWidth();

	int GetHeight();

	bool IsOpen();

	void Update(float dt);

	bool Is(std::string type);

	void SetScale(float scaleX, float scaleY);

	Vec2 GetScale();

	void SetFrame(int frame);

	void SetFrameCount(int frameCount);

	void SetFrameTime(float frameTime);


private:

	SDL_Texture* texture;

	int width = 0;

	int height = 0;

	SDL_Rect clipRect;

	Vec2 scale;

	int frameCount;

	int currentFrame;

	float timeElapsed;

	float frameTime;

	Timer selfDestructCount;

	float secondsToSelfDestruct;
};



#endif /* SOURCE_HEADERS_SPRITE_H_ */
