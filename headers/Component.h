/*
 * Component.h
 *
 *  Created on: Apr 6, 2018
 *      Author: TitoKlautau
 */

#ifndef SOURCE_HEADERS_COMPONENT_H_
#define SOURCE_HEADERS_COMPONENT_H_

#include<string>
#include "GameObject.h"

class GameObject;

class Component{
public:
	Component(GameObject& associated);

	virtual ~Component();

	virtual void Update(float dt) = 0;

	virtual void Render() = 0;

	virtual bool Is(std::string type) = 0;

	virtual void Start();

	virtual void NotifyCollision(GameObject& other);

protected:
	GameObject& associated;
};
#endif /* SOURCE_HEADERS_COMPONENT_H_ */
