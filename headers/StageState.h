/*
 * State.h
 *
 *  Created on: Mar 20, 2018
 *
 */

#ifndef SOURCE_HEADERS_STAGESTATE_H_
#define SOURCE_HEADERS_STAGESTATE_H_

#define INCLUDE_SDL

#include "SDL_Include.h"
#include <iostream>
#include <vector>
#include <memory>

#include "Music.h"
#include "Sprite.h"
#include "State.h"
#include "TileSet.h"


class StageState: public State{
public:

	StageState();

	~StageState();

	void LoadAssets();
	void Update(float dt);
	void Render();

	void Start();
	void Pause();
	void Resume();

private:

	Music backgroundMusic;
	//TileSet* tileset;

};

#endif /* SOURCE_HEADERS_STAGESTATE_H_ */
