/*
 * TitleState.h
 *
 *  Created on: May 15, 2018
 *      Author: TMK
 */

#ifndef SOURCE_HEADERS_TITLESTATE_H_
#define SOURCE_HEADERS_TITLESTATE_H_

#include "State.h"
#include "Timer.h"

class TitleState:public State{
public:

	TitleState();
	~TitleState();

	void LoadAssets();
	void Update(float dt);
	void Render();

	void Start();
	void Pause();
	void Resume();

private:

	Timer textTimer;

};



#endif /* SOURCE_HEADERS_TITLESTATE_H_ */
