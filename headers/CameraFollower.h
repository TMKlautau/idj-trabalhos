/*
 * CameraFollower.h
 *
 *  Created on: May 4, 2018
 *      Author: TMK
 */

#ifndef SOURCE_HEADERS_CAMERAFOLLOWER_H_
#define SOURCE_HEADERS_CAMERAFOLLOWER_H_

#include "GameObject.h"

class Component;

class CameraFollower: public Component{
public:
	CameraFollower(GameObject& associated);

	void Update(float dt);

	void Render();

	bool Is(std::string type);
};



#endif /* SOURCE_HEADERS_CAMERAFOLLOWER_H_ */
