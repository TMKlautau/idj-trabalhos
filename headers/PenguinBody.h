/*
 * PenguinBody.h
 *
 *  Created on: May 10, 2018
 *      Author: TMK
 */

#ifndef SOURCE_HEADERS_PENGUINBODY_H_
#define SOURCE_HEADERS_PENGUINBODY_H_

#include "Component.h"
#include "GameObject.h"
#include <memory>

class PenguinBody:public Component{
public:
	PenguinBody(GameObject& associated);
	~PenguinBody();

	void Start();

	void Update(float dt);

	void Render();

	bool Is(std::string type);

	void NotifyCollision(GameObject& other);

	static PenguinBody* Player;

	Vec2 GetPosition();
private:

	std::weak_ptr<GameObject> pcannon;

	Vec2 speed;

	float linearSpeed;

	float angle;

	int hp;
};



#endif /* SOURCE_HEADERS_PENGUINBODY_H_ */
