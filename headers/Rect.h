/*
 * Rect.h
 *
 *  Created on: Mar 22, 2018
 *      Author: TMK
 */

#ifndef SOURCE_HEADERS_RECT_H_
#define SOURCE_HEADERS_RECT_H_

#include "Vec2.hpp"

class Rect{

public:

	explicit Rect(float x=0, float y=0, float w=0, float h=0);

	~Rect();

	void SumVec2(float* Xresult, float* Yresult, float* Wresult, float* Hresult, float xTerm, float yTerm);

	Vec2 RectCenter();

	float DistCenterR(Rect* rectTerm);

	bool Contains(float xTerm, float yTerm);

	float x, y, w, h;

};



#endif /* SOURCE_HEADERS_RECT_H_ */
