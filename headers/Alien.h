/*
 * Alien.h
 *
 *  Created on: May 5, 2018
 *      Author: TMK
 */

#ifndef SOURCE_HEADERS_ALIEN_H_
#define SOURCE_HEADERS_ALIEN_H_

#include "GameObject.h"
#include "Vec2.hpp"
#include <queue>
#include <vector>
#include <memory>
#include "Timer.h"


const float ALIENMAXSPEED = 300.f;

class Component;

enum AlienState {MOVING, RESTING};

class Alien: public Component{
public:

	Alien(GameObject& associated, int nMinions, float timeOffset = 0);

	~Alien();

	void Start();

	void Update(float dt);

	void Render();

	bool Is(std::string type);

	void NotifyCollision(GameObject& other);

	static int alienCount;

private:

	Vec2 speed;
	int hp;
	int nMinions;

	AlienState state;

	Timer RestTimer;

	Vec2 destination;

	std::vector<std::weak_ptr<GameObject>> minionArray;

	float timeOffset;

};


#endif /* SOURCE_HEADERS_ALIEN_H_ */
