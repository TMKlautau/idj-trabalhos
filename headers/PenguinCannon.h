/*
 * PenguinCannon.h
 *
 *  Created on: May 10, 2018
 *      Author: TMK
 */

#ifndef SOURCE_HEADERS_PENGUINCANNON_H_
#define SOURCE_HEADERS_PENGUINCANNON_H_

#include "Component.h"
#include "GameObject.h"
#include <memory>
#include "Timer.h"

class PenguinCannon:public Component{
public:

	PenguinCannon(GameObject& associated, std::weak_ptr<GameObject> penguinBody);

	void Update(float dt);

	void Render();

	bool Is(std::string type);

	void Shoot();

	void Start();

private:

	std::weak_ptr<GameObject> pbody;

	float angle;

	Timer timer;

};



#endif /* SOURCE_HEADERS_PENGUINCANNON_H_ */
