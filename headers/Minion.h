/*
 * Minion.h
 *
 *  Created on: May 9, 2018
 *      Author: TMK
 */

#ifndef SOURCE_HEADERS_MINION_H_
#define SOURCE_HEADERS_MINION_H_

#include "Component.h"
#include <memory>

class Minion: public Component{
public:

	Minion(GameObject& associated, GameObject& alienCenter, float arcOffsetDeg = 0);

	void Update(float dt);

	void Render();

	bool Is(std::string type);

	void Shoot(Vec2 target);

	void Start();

private:

	GameObject& alienCenter;

	float arc;

};



#endif /* SOURCE_HEADERS_MINION_H_ */
