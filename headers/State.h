/*
 * State.h
 *
 *  Created on: May 15, 2018
 *      Author: TMK
 */

#ifndef SOURCE_HEADERS_STATE_H_
#define SOURCE_HEADERS_STATE_H_

#include <memory>
#include "GameObject.h"
#include <vector>

class State{
public:
	State();
	virtual ~State();
	State(State&&) = default;
	State(const State&) = delete;

	virtual void LoadAssets() = 0;
	virtual void Update(float dt) = 0;
	virtual void Render() = 0;

	virtual void Start() = 0;
	virtual void Pause() = 0;
	virtual void Resume() = 0;

	virtual std::weak_ptr<GameObject> AddObject(GameObject* object);
	virtual std::weak_ptr<GameObject> GetObjectPtr(GameObject* object);

	bool PopRequested();
	bool QuitRequested();

protected:

	void StartArray();
	virtual void UpdateArray(float dt);
	virtual void RenderArray();

	bool popRequested;
	bool quitRequested;
	bool started;

	std::vector<std::shared_ptr<GameObject>> objectArray;
};




#endif /* SOURCE_HEADERS_STATE_H_ */
